.LOG

This repository is for projects related to talks given at the
Orange County Unity3D Meetup group, located here:

https://www.meetup.com/Orange-County-Unity-3D-Developers/

See Unity Asset store for license on their assets.

The Unity3D 2D Roguelike tutorial (original) and assets can be found here:

https://unity3d.com/learn/tutorials/s/2d-roguelike-tutorial

https://www.assetstore.unity3d.com/en/#!/content/29825
